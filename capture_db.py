from __future__ import print_function

import boto3
import json

print('Loading function')
CAPTURE_TABLE_NAME = 'CaptureHistory'
DYNAMO = boto3.resource('dynamodb').Table(CAPTURE_TABLE_NAME)

def capture_history_handler(event, context):
    '''Provide an event that contains the following keys:

      - operation: one of the operations in the operations dict below
      - payload: a parameter to pass to the operation being performed
    '''
    #print("Received event: " + json.dumps(event, indent=2))
    # tableName is fixed for this lambda function

    operation = event['operation']

    operations = {
        'upsert': lambda x: upsert(x),
        'get': lambda x: get(x),
        'update': lambda x: DYNAMO.update_item(**x),
        'delete': lambda x: DYNAMO.delete_item(**x),
        'listAll': lambda x: listAll(),
        'echo': lambda x: x
    }

    if operation in operations:
        return operations[operation](event.get('payload'))
    else:
        raise ValueError('Unrecognized operation "{}"'.format(operation))
        
def get(item):
    read_operation = { 'TableName': CAPTURE_TABLE_NAME, 'Key': item }
    print('Get event: ' + json.dumps(read_operation, indent=2))
    return DYNAMO.get_item(**read_operation)['Item']
    
def upsert(item):
    write_operation = { 'TableName': CAPTURE_TABLE_NAME, 'Item': item }
    print('Put event:' + json.dumps(write_operation, indent=2))
    return DYNAMO.put_item(**write_operation)
    
def listAll():
    list_operation = { 'TableName': CAPTURE_TABLE_NAME }
    return DYNAMO.scan(**list_operation)['Items']
    